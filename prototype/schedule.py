import schedule
import time
import threading
from test_notifier import notify

def job():
    print("I'm working... {0}".format(threading.current_thread()))
    notify(title    = 'Notifier',
           subtitle = 'Task to do !!!',
           message  = 'Your message')

def schedule_task(job_func):
    job_thread = threading.Thread(target=job_func)
    job_thread.start()

t="21:22"
schedule.every(1).second.do(schedule_task, job)
#schedule.every().day.at(t).do(schedule_task, job)

while True:
    schedule.run_pending()
    time.sleep(1)
