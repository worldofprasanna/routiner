from routiner import get_task_list

def test_read_yml():
    actual_tasks = get_task_list()
    expected_tasks = [{'Get up in the morning': '05:30'},
                      { 'Note daily expense':'21:00'}]

    assert expected_tasks == actual_tasks
