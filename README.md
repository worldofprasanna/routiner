Spec:

    . Mac growl type notification for the daily routines
    . Link or referer to easily take you to that place (eg: Read news -> http://news.google.com, https://news.ycombinator.com)
    . Able to set the remainders easily
    . Able to mark whether its done or not doing it today
    . Progress for the month
    . Homepage to explain these things


Setup:

To setup the project,

-	Install mkvirtualenv and create a new virtual environment
	`mkvirtualenv routiner`
-	Run `pip install -r requirements.txt`

To run the test,

-	`py.test`
-	For TDD, its best to have test watch. Use this to run test continuously and to notify about the result when file changes `ptw --onpass "say super" --onfail "say poda madaya"`
