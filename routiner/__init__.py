import yaml
import schedule
import time
import threading
import os

def get_task_list():
    with open('routiner/example_task.yml', 'r') as f:
        doc = yaml.load(f)
        tasks = doc['tasks']
    return tasks

def main():
    tasks = get_task_list()
    for task in tasks:
        [job_scheduler(task_name, notification_time) for (task_name, notification_time) in task.items()]
    while 1:
        schedule.run_pending()
        time.sleep(1)

def notify(title, subtitle, message):
    t = '-title {!r}'.format(title)
    s = '-subtitle {!r}'.format(subtitle)
    m = '-message {!r}'.format(message)
    os.system('terminal-notifier {}'.format(' '.join([m, t, s])))

def schedule_task(job_func, task_name):
    job_thread = threading.Thread(target=job_func, args=(task_name, ))
    job_thread.start()

def job_scheduler(task_name, notification_time):
    schedule.every().day.at(notification_time).do(schedule_task, job, task_name)

def job(task_name):
    notify(
        title = 'Routiner !!!',
        subtitle = task_name,
        message = 'Its a remainder'
    )

#main()
